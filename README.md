## Create automatic backup save

1. Create the file `~/.backuprc` and copy the following :

  ```
  # serveur destinataire
  BKSV=ses-epione

  # repertoire recepteur. Le creer sur BKSV s il n existe pas.
  BKDESTDIR=/user/$USER/ses/ses/BackupHome

  # repertoire recepteur alternatif. Le creer sur BKSV s il n existe pas.
  BKINCRDIR=/user/$USER/ses/ses/IncrHome

  # repertoires locaux a synchroniser vers BKDESTDIR
  BKSRCDIR=/user/$USER/home/Documents/

  # BKDESTUSER=$USER                # loginname a faire paraitre dans l article syslog.
  # MAILDEST=$USER                  # Adresse mail recepteur des Mails
  # KEEPTIME=8                      # Durée de rétention dans espace alternatif. Defaut: 8
  # BKLOG=$HOME/.lastbackup         # fichier de log dernière exécution de backup
  SMTPRELAY=smtp.inria.fr           # serveur smtp pour envoie du mail d'erreur
  # MAILDOMAIN=inria.fr
  # MAILDEST=$USER@sophia.inria.fr
  # MAILFROM=$USER@sophia.inria.fr
  ```

2. Create the folders `BackupHome` and `IncrHome` at `/user/$USER/ses/ses/`.

3. Create a crontab with `crontab -e` and copy the following :
  ```
  # Tuesday and Friday at 3pm
  0 15 * * 2,5 /usr/local/bashutil/bin/backup
  ```

4. The script `backup` called by crontab will execute your `.backuprc` and save `BKSRCDIR` into `BKDESTDIR`.
